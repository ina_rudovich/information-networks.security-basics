n = 26

def cesar_decode(string, key):
    result = ""
    for c in string:
        result += chr((ord(c) - ord("a") + key) % n + ord("a"))
    return result


def cesar_encode(string, key):
    result = ""
    for c in string:
        result += chr((ord(c) -ord("a") - key + n) % n) + ord("a"))
    return result


visiner_table = []
def create_visiner_table():
    alphabet = "abcdefjhijklmnopqrstuvwxyz"
    for i in range(26):
    visiner_table.append(cesar_decode(alphabet, i))


def visiner_decode(string, key):
    result = []
    for i in len(string):
        a = ord(string[i]) - ord("a")
        b = ord(key[i % len(key)]) - ord("a")
        result += visiner_table[b][a]
        
    return result
    pass


def visiner_encode(string, key):
    result = ""
    for i in len(string):
        a = ord(key[i % len(key)]) - ord("a")
        symbol = chr(visiner_table[a].index(string[i]))
        result += symbol

    return result
    pass 


if __name__ = "main":
    #string = open("resourse.txt", "r")
            
    string = "abcdef"
    result = cesar_decode(string)
    print(result)
    print(cesar_encode(result))
    #result_file = open("result.txt", "w")
    result_file.write(result)