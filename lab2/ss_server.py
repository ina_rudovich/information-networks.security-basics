import time
from random import randint

from des import des_encryption, des_decryption

class ServerSS(object):

    def __init__(self):
        self.identifier = str(randint(1000000, 9000000))
        self.K_tgs_ss = str(randint(1000000, 9000000))

    def response_to_client(self, message):
        encrypt_tgt, encrypt_aut = message.split(";")

        tgs = des_decryption(encrypt_tgt, self.K_tgs_ss)

        client_identifier, tgs_identifier, t1, timeout, K_c_ss = tgs.split(";")

        AUT = des_decryption(encrypt_aut, K_c_ss)
        client_identifier_2, t4 = AUT.split(";")

        if client_identifier != client_identifier_2:
            raise Exception("WRONG CLIENT")
        if int(t1) + int(timeout) < int(t4):
            raise Exception("TIMEOUT")

        return des_encryption(str(int(t4) + 1), K_c_ss)

