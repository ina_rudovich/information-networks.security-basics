import time
from random import randint

from des import des_encryption, des_decryption

class Client(object):

    def __init__(self):
        self.identifier = "client"
        self.key = "1111111"
        self.K_as_tgs = None


    def send_identifier_to_AS(self):
        return self.identifier

    def request_to_tgs(self, tgt):
        decrypted_tgt = des_decryption(tgt, self.key)
        tgt, self.K_c_tgs = decrypted_tgt.split(';')

        AUT = self.identifier + ";" + str(int(time.time()))
        encrypted_AUT = des_encryption(AUT, self.K_c_tgs)

        response = tgt + ";" + encrypted_AUT + ";" + self.identifier
        return response

    def request_to_ss(self, message):

        encrypt_tgt, self.K_c_ss = des_decryption(message,self.K_c_tgs).split(";")

        self.time = str(int(time.time()))
        AUT = self.identifier + ";" + self.time


        response = encrypt_tgt + ";" + des_encryption(AUT, self.K_c_ss)
        return response

    def verificate_server(self, message):
        t = des_decryption(message, self.K_c_ss)
        if int(t) - 1 != int(self.time):
            raise Exception("NOT VERIFIED SERVER")
