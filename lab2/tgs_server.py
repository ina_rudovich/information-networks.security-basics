import time
from random import randint

from des import des_encryption, des_decryption

class  TicketGivingServer(object):
    def __init__(self, K_as_tgs):
        self.identifier = "tgs0000"
        self.K_as_tgs = K_as_tgs
        self.K_tgs_ss = None

    def responce_for_client(self, message):
        tgt, encrypted_AUT, client_identifier = message.split(";")
        decrypted_tgt = des_decryption(tgt, self.K_as_tgs)
        real_client_identifier, tgs_identifier, t1, timeout, K_c_tgs = decrypted_tgt.split(";")
        AUT = des_decryption(encrypted_AUT, K_c_tgs)
        client_identifier, t2 = AUT.split(";")
        if real_client_identifier == client_identifier:
            if int(t1) + int(timeout) < int(t2):
                raise Exception("TIMEOUT")
        else:
            raise Exception("DIFFERENT CLIENTS")

        # K_c_ss = str(randint(1000000, 9999999))
        K_c_ss = "3333333"
        tgs = client_identifier + ";" + \
                self.identifier + ";" + \
                str(int(time.time())) + ";" + \
                "100" + ";" + \
                K_c_ss

        response = des_encryption(des_encryption(tgs, self.K_tgs_ss)+";"+K_c_ss, K_c_tgs)
        return response
