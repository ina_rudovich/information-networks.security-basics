import time
from random import randint

from des import des_encryption, des_decryption

from client import Client
from as_server import AutentificationServer
from tgs_server import TicketGivingServer
from ss_server import ServerSS


def main():
    C = Client()
    SS = ServerSS()
    AS = AutentificationServer()
    TGS = TicketGivingServer(AS.K_as_tgs)

    AS.TGS_identifier = TGS.identifier
    AS.add_client(C.identifier, C.key)

    # 1
    client_identifier = C.send_identifier_to_AS()
    print("message sent to as_server")
    # 2
    TGT = AS.get_tgt(client_identifier)
    print("as_server responsed to client")
    # 3
    message = C.request_to_tgs(TGT)
    print("message sent to tgs_server")
    # 4
    TGS.K_tgs_ss = SS.K_tgs_ss
    message = TGS.responce_for_client(message)
    print("tgs_server responsed to client")
    # 5
    message = C.request_to_ss(message)
    print("message sent to ss_server")
    # 6
    message = SS.response_to_client(message)
    print("ss_server responsed")

    C.verificate_server(message)
    print("ss server verified")


if __name__ == "__main__":
    main()
