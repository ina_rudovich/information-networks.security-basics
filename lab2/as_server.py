import time
from random import randint

from des import des_encryption, des_decryption

class  AutentificationServer(object):
    def __init__(self):
        self.clients = {}
        self.TGS_identifier = None
        # self.K_as_tgs = str(randint(1000000,9999999))
        self.K_as_tgs = "0000000"

    def add_client(self, client_identifier, key):
        self.clients[client_identifier] = key

    def get_tgt(self, client_identifier):
        if not client_identifier in self.clients.keys():
            raise Exception("NO SUCH CLIENT")

        # K_c_tgs = str(randint(1000000, 9999999))
        K_c_tgs = "2222222"
        timeout = '100'
        tgt = client_identifier + ";" +\
              self.TGS_identifier+ ";" + \
              str(int(time.time())) + ";" + \
              timeout + ";" + \
              K_c_tgs

        response = des_encryption(tgt, self.K_as_tgs) + ";" + K_c_tgs

        return des_encryption(response, self.clients[client_identifier])

