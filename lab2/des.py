# -*- coding: utf-8 -*-

# first permutation in the per-round key generation
key_permutation = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51,
         43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7,
         62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20,
         12, 4]

# Shift Matrix for each round of keys
SHIFTS = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1]

# second permutation in the per-round key generation
key_permutation_2= [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16,
         7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44,
         49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32]

# initial permutation of the data
initial_permutation = [58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62,
               54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49,
               41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45,
               37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7]

# expansion permutation of the data
expansion_permutation = [32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12,
                 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24,
                 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1]

# S-boxes
S1 = [[14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
      [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
      [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
      [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]]

S2 = [[15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
      [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
      [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
      [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]]

S3 = [[10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
      [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
      [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
      [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]]

S4 = [[7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
      [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
      [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
      [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]]

S5 = [[2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
      [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
      [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
      [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]]

S6 = [[12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
      [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
      [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
      [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]]

S7 = [[4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
      [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
      [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
      [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]]

S8 = [[13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
      [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
      [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
      [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]]

S = [S1, S2, S3, S4, S5, S6, S7, S8]


# permutation of B[j]
p_permutation = [16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8, 24,
         14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25]

final_permutation = [40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31, 38,
             6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4,
             44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42,
             10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25]


def to_bin(char):
    b = str(bin(ord(char)))[2:]
    return("0"*(8-len(b)) + b)


def convert_str_to_bit_array(string):
    bit_array = ""
    for char in string:
        bit_array += to_bin(char)
    bit_array = [int(i) for i in bit_array]
    return bit_array


def convert_bit_array_to_str(bit_array):
    bit_array = "".join([str(i) for i in bit_array])
    result = ""
    for i in range(0, len(bit_array), 8):
        number = bit_array[i: i+8]
        result += chr(int(number, 2))

    return result


def XOR(l, r):
    return [l[i]^r[i] for i in range(len(l))]


def expansion_function(R):
    result = []
    for i in expansion_permutation:
        result.append(R[i-1])
    return result


def feistel_function(R, k):
    R_e = expansion_function(R)
    R_k = XOR(R_e, k)

    B = [[]] * 8
    for i in range(8):
        B[i] = R_k[i*6:i*6+6]

    B_ = [[]] * 8
    for i in range(8):
        a = 2 * B[i][0] + B[i][-1]
        b = 8 * B[i][1] + 4 * B[i][2] + 2 * B[i][3] + B[i][4]
        B_[i] = [int(i) for i in to_bin(chr(S[i][a][b]))[4:]]

    result = []
    for i in range(8):
        result.extend(B_[i])

    result  = [result[i-1] for i in p_permutation]

    return  result


def generate_key(key):
    extended_key = []
    for i in range(8):
        byte = key[i*7:i*7+7]
        extended_key.extend(byte)
        if byte.count(1) % 2:
            extended_key.append(0)
        else:
            extended_key.append(1)

    key_0 = [0]*56
    for i in range(56):
        key_0[i] = extended_key[key_permutation[i]-1]

    keys = [key_0]
    C = keys[-1][:28]
    D = keys[-1][28:]
    for i in range(16):
        C = C[SHIFTS[i]:] + C[:SHIFTS[i]]
        D = D[SHIFTS[i]:] + D[:SHIFTS[i]]
        key_i = C + D
        key_i = [key_i[i-1] for i in key_permutation_2]
        keys.append(key_i)
    return keys[1:]


def _des_encryption(bit_array, key):
    keys = generate_key(key)

    T = []
    for i in initial_permutation:
        T.append(bit_array[i-1])

    L = [None] * 17
    R = [None] * 17
    for i in range(16):
        L[i] = T[:32]
        R[i] = T[32:]
        L[i+1] = R[i]
        R[i+1] = XOR(L[i], feistel_function(R[i], keys[i]))
        T = L[i+1] + R[i+1]

    T = R[16] + L[16]

    result = [T[i-1] for i in final_permutation]
    return result


def _des_decryption(bit_array, key):
    keys = generate_key(key)
    keys.reverse()

    T = []
    for i in initial_permutation:
        T.append(bit_array[i - 1])

    L = [None] * 17
    R = [None] * 17
    for i in range(16):
        L[i] = T[:32]
        R[i] = T[32:]
        L[i + 1] = R[i]

        feistel_function(R[i], keys[i])
        R[i + 1] = XOR(L[i], feistel_function(R[i], keys[i]))

        T = L[i + 1] + R[i + 1]

    T = R[16] + L[16]

    result = [T[i - 1] for i in final_permutation]
    return result


def des_encryption(string, key):
    bit_array = convert_str_to_bit_array(string)

    key_bits = convert_str_to_bit_array(key)

    encrypted_bits = []
    for i in range(len(bit_array) // 64):
        encrypted_blok = _des_encryption(bit_array[i*64:(i+1)*64], key_bits[:56])
        encrypted_bits += encrypted_blok
    encrypted_bits +=bit_array[len(bit_array) - len(bit_array) % 64:]

    return convert_bit_array_to_str(encrypted_bits)


def des_decryption(string, key):
    bit_array = convert_str_to_bit_array(string)

    key_bits = convert_str_to_bit_array(key)

    decrypted_bits = []
    for i in range(len(bit_array) // 64):
        decrypted_blok = _des_decryption(bit_array[i*64:(i+1)*64], key_bits[:56])
        decrypted_bits += decrypted_blok
    decrypted_bits +=bit_array[len(bit_array) - len(bit_array) % 64:]
    return convert_bit_array_to_str(decrypted_bits)


if __name__ == '__main__':
    string = "client;tgs0000;1519901994;100;2222222"
    print(string)
    key =  "0000000"

    encrypted_string = des_encryption(des_encryption(string, key),key)
    print(encrypted_string)
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    decrypted_string = des_decryption(des_decryption(encrypted_string, key),key)
    print(decrypted_string)
